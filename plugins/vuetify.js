import '@mdi/font/css/materialdesignicons.css' // Ensure you are using css-loader
import Vue from 'vue'
import Vuetify from 'vuetify/lib'
import colors from 'vuetify/es5/util/colors'

Vue.use(Vuetify, {
  iconfont: 'md',
  theme: {
    primary: '#6135ae',
    accent: '#ab7ffb',
    secondary: '#fffa9b',
    info: 'cdffbb',
    warning: '#feeb57',
    error: colors.deepOrange.accent4,
    success: '#3d3d3d'
  }
})
