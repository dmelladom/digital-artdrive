# digital-artdrive

> Store your art collection in a reliable place

## Build Setup

``` bash
# install dependencies
$ yarn install

# serve with hot reload at localhost:3000
$ yarn run dev

# build for production and launch server
$ yarn run build
$ yarn start

# generate static project
$ yarn run generate
```

For detailed explanation on how things work, checkout [Nuxt.js docs](https://nuxtjs.org).

## Acknowledgements

Proyecto desarrollado con cargo a las Ayudas a la Modernización e Innovación en las Industrias Culturales y Creativas mediante proyectos digitales y tecnológicos, convocatoria 2018 

Project developed thanks to the Aid to the Modernization and Innovation of Cultural and Creative Industrias by the means of digital and technological projects, 2018

![logo ministerio](https://www.digitalartdrive.com/logoministerio.jpg)
